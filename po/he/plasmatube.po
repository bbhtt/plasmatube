# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasmatube package.
#
# SPDX-FileCopyrightText: 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasmatube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-14 00:38+0000\n"
"PO-Revision-Date: 2024-04-08 06:47+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.5\n"

#: api/invidious/invidiousapi.cpp:110 api/piped/pipedapi.cpp:84
#: controllers/logincontroller.cpp:37
#, kde-format
msgid "Username or password is wrong."
msgstr "שם המשתמש או הסיסמה שגויים."

#: api/invidious/invidiousapi.cpp:400 api/peertube/peertubeapi.cpp:380
#: api/piped/pipedapi.cpp:318
#, kde-format
msgid "Server returned no valid JSON."
msgstr "השרת לא החזיר JSON תקין."

#: api/invidious/invidiousapi.cpp:409 api/peertube/peertubeapi.cpp:389
#: api/piped/pipedapi.cpp:327
#, kde-format
msgid "Server returned the status code %1"
msgstr "השרת החזיר את קוד המצב %1"

#: controllers/logincontroller.cpp:40
#, kde-format
msgid "This instance has disabled the registration."
msgstr "במופע הזה הושבתה ההרשמה."

#: controllers/videocontroller.cpp:62
#, kde-format
msgid "Playing video"
msgstr "סרטון מתנגן"

#: main.cpp:54
#, kde-format
msgid "PlasmaTube"
msgstr "PlasmaTube"

#: main.cpp:56
#, kde-format
msgid "YouTube client"
msgstr "לקוח ל־YouTube"

#: main.cpp:58
#, kde-format
msgid "© 2019-2024 Linus Jahn, 2019-2024 KDE Community"
msgstr "© 2019‏-2024 לינוס יאהן, 2019‏-2024 קהילת KDE"

#: main.cpp:59
#, kde-format
msgid "Linus Jahn"
msgstr "לינוס יאן"

#: main.cpp:59
#, kde-format
msgid "Creator"
msgstr "יוצר"

#: main.cpp:60
#, kde-format
msgid "Joshua Goins"
msgstr "ג׳ושוע גוינס"

#: main.cpp:61
#, kde-format
msgid "Maintainer"
msgstr "מתחזק"

#: main.cpp:65
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "צוות התרגום של KDE ישראל"

#: main.cpp:65
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-l10n-he@kde.org"

#: models/videolistmodel.cpp:18 ui/components/Sidebar.qml:101
#, kde-format
msgid "Subscriptions"
msgstr "מינויים"

#: models/videolistmodel.cpp:20
#, kde-format
msgid "Popular"
msgstr "נפוצים"

#: models/videolistmodel.cpp:22
#, kde-format
msgctxt "@action:button All trending videos"
msgid "All"
msgstr "הכול"

#: models/videolistmodel.cpp:24
#, kde-format
msgctxt "@action:button Trending gaming videos"
msgid "Gaming"
msgstr "משחקים"

#: models/videolistmodel.cpp:26
#, kde-format
msgctxt "@action:button Trending movie videos"
msgid "Movies"
msgstr "סרטים"

#: models/videolistmodel.cpp:28
#, kde-format
msgctxt "@action:button Trending music videos"
msgid "Music"
msgstr "מוזיקה"

#: models/videolistmodel.cpp:30
#, kde-format
msgctxt "@action:button Trending news videos"
msgid "News"
msgstr "חדשות"

#: models/videolistmodel.cpp:32
#, kde-format
msgctxt "@action:button Video watch history"
msgid "History"
msgstr "היסטוריה"

#: models/videolistmodel.cpp:160
#, kde-format
msgid "Search results for \"%1\""
msgstr "תוצאות לחיפוש אחר „%1”"

#: ui/android/ShareAction.qml:15 ui/android/ShareMenu.qml:15
#: ui/components/ShareAction.qml:17 ui/components/ShareMenu.qml:15
#, kde-format
msgid "Share"
msgstr "שיתוף"

#: ui/android/ShareAction.qml:16 ui/components/ShareAction.qml:18
#, kde-format
msgid "Share the selected media"
msgstr "שיתוף המדיה הנבחרת"

#: ui/ChannelPage.qml:36 ui/FeedPage.qml:47 ui/PlaylistPage.qml:35
#: ui/SearchPage.qml:160 ui/settings/NetworkProxyPage.qml:45
#, kde-format
msgid "Proxy Settings"
msgstr "הגדרות מתווך"

#: ui/ChannelPage.qml:142
#, kde-format
msgctxt "@item:inmenu"
msgid "Videos"
msgstr "סרטונים"

#: ui/ChannelPage.qml:148
#, kde-format
msgctxt "@item:inmenu"
msgid "Playlists"
msgstr "רשימות נגינה"

#: ui/components/BaseGridView.qml:157
#, kde-format
msgid "No videos"
msgstr "אין סרטונים"

#: ui/components/BottomNavBar.qml:70
#, kde-format
msgid "Videos"
msgstr "סרטונים"

#: ui/components/BottomNavBar.qml:82 ui/loginflow/AddInvidiousPage.qml:34
#: ui/loginflow/AddPeerTubePage.qml:33 ui/loginflow/AddPipedPage.qml:34
#: ui/SearchPage.qml:17
#, kde-format
msgid "Search"
msgstr "חיפוש"

#: ui/components/BottomNavBar.qml:93 ui/videoplayer/VideoControls.qml:306
#, kde-format
msgid "Settings"
msgstr "הגדרות"

#: ui/components/BottomNavBar.qml:98 ui/components/Sidebar.qml:156
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "הגדרות"

#: ui/components/Comments.qml:97
#, kde-format
msgctxt "@action:button Load more comments"
msgid "Load more"
msgstr "לטעון עוד"

#: ui/components/PlaylistGridItem.qml:53
#, kde-format
msgid "%1 video"
msgid_plural "%1 videos"
msgstr[0] "סרטון"
msgstr[1] "שני סרטונים"
msgstr[2] "%1 סרטונים"
msgstr[3] "%1 סרטונים"

#: ui/components/ShareDialog.qml:33
#, kde-format
msgid "Sharing failed"
msgstr "השיתוף נכשל"

#: ui/components/Sidebar.qml:85
#, kde-format
msgid "Trending"
msgstr "מובילים"

#: ui/components/Sidebar.qml:118 ui/PlaylistsPage.qml:13
#, kde-format
msgid "Playlists"
msgstr "רשימות נגינה"

#: ui/components/Sidebar.qml:135
#, kde-format
msgid "History"
msgstr "היסטוריה"

#: ui/components/Sidebar.qml:152
#, kde-format
msgctxt "@action:button Open settings dialog"
msgid "Settings"
msgstr "הגדרות"

#: ui/components/SourceSwitcher.qml:48 ui/settings/SourcesPage.qml:64
#, kde-format
msgid "Invidious"
msgstr "Invidious"

#: ui/components/SourceSwitcher.qml:50 ui/settings/SourcesPage.qml:66
#, kde-format
msgid "PeerTube"
msgstr "PeerTube"

#: ui/components/SourceSwitcher.qml:52 ui/settings/SourcesPage.qml:68
#, kde-format
msgid "Piped"
msgstr "Piped"

#: ui/components/SourceSwitcher.qml:64
#: ui/components/SourceSwitcherDialog.qml:28
#, kde-format
msgid "Switch Source"
msgstr "החלפת מקור"

#: ui/components/SourceSwitcherDialog.qml:55
#, kde-format
msgid "Add Source"
msgstr "הוספת מקור"

#: ui/components/SourceSwitcherDialog.qml:59
#, kde-format
msgid "Add a new video source"
msgstr "הוספת מקור וידאו חדש"

#: ui/components/SubscriptionButton.qml:61
#, kde-format
msgctxt ""
"@action:button Unsubscribe to this channel, %1 is the sub count if provided"
msgid "Unsubscribe%1"
msgstr "ביטול הרשמה%1"

#: ui/components/SubscriptionButton.qml:63
#, kde-format
msgctxt ""
"@action:button Subscribe to this channel, %1 is the sub count if provided"
msgid "Subscribe%1"
msgstr "הרשמה%1"

#: ui/components/SubscriptionButton.qml:73
#, kde-format
msgid "Please log in to subscribe to channels."
msgstr "נא להיכנס כדי להירשם לערוצים."

#: ui/components/VideoGridItem.qml:67
#, kde-format
msgctxt "Short label for a livestream that's live"
msgid "Live"
msgstr "שידור חי"

#: ui/components/VideoGridItem.qml:74
#, kde-format
msgctxt "Short label for a short video"
msgid "Short"
msgstr "Short"

#: ui/components/VideoGridItem.qml:152 ui/components/VideoListItem.qml:144
#, kde-format
msgid "No views"
msgstr "אין צפיות"

#: ui/components/VideoGridItem.qml:154 ui/components/VideoListItem.qml:146
#: ui/videoplayer/VideoPlayer.qml:378
#, kde-format
msgid "%1 views"
msgstr "%1 צפיות"

#: ui/components/VideoGridItem.qml:157
#, kde-format
msgid "<i>live now</i>"
msgstr "<i>חי כרגע</i>"

#: ui/components/VideoListItem.qml:156
#, kde-format
msgid " • %1"
msgstr " • %1"

#: ui/components/VideoMenu.qml:31
#, kde-format
msgid "Play Now"
msgstr "לנגן כעת"

#: ui/components/VideoMenu.qml:31
#, kde-format
msgid "Play"
msgstr "נגינה"

#: ui/components/VideoMenu.qml:41
#, kde-format
msgid "Play Next"
msgstr "הבא לניגון"

#: ui/components/VideoMenu.qml:50
#, kde-format
msgid "Play in Picture-in-Picture"
msgstr "נגינה בחלונית צפה"

#: ui/components/VideoMenu.qml:64
#, kde-format
msgid "Mark as Unwatched"
msgstr "סימון כלא נצפה"

#: ui/components/VideoMenu.qml:64
#, kde-format
msgid "Mark as Watched"
msgstr "סימון כנצפה"

#: ui/components/VideoMenu.qml:78
#, kde-format
msgid "Add to Playlist…"
msgstr "הוספה לרשימת נגינה…"

#: ui/components/VideoMenu.qml:85
#, kde-format
msgid "Remove from Playlist"
msgstr "הסרה מרשימת הנגינה"

#: ui/FeedPage.qml:35
#, kde-format
msgctxt "@info:status Network status"
msgid "Failed to contact server: %1. Please check your proxy settings."
msgstr "התקשורת נכשלה מול השרת: %1. נא לבדוק את הגדרות המתווך שלך."

#: ui/loginflow/AddInvidiousPage.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Add Invidious Source"
msgstr "הוספת מקור Invidious"

#: ui/loginflow/AddInvidiousPage.qml:28 ui/loginflow/AddPeerTubePage.qml:27
#: ui/loginflow/AddPipedPage.qml:28
#, kde-format
msgid "Select a instance"
msgstr "בחירת מופע"

#: ui/loginflow/AddInvidiousPage.qml:35
#, kde-format
msgid "invidious.io"
msgstr "invidious.io"

#: ui/loginflow/AddInvidiousPage.qml:68 ui/loginflow/AddPeerTubePage.qml:64
#: ui/loginflow/AddPipedPage.qml:69
#, kde-format
msgid "No public instances found."
msgstr "לא נמצאו מופעים ציבוריים."

#: ui/loginflow/AddPeerTubePage.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Add PeerTube Source"
msgstr "הוספת מקור PeerTube"

#: ui/loginflow/AddPeerTubePage.qml:34
#, kde-format
msgid "joinpeertube.org"
msgstr "joinpeertube.org"

#: ui/loginflow/AddPipedPage.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Add Piped Source"
msgstr "הוספת מקור Piped"

#: ui/loginflow/AddPipedPage.qml:35
#, kde-format
msgid "pipedapi.kavin.rocks"
msgstr "pipedapi.kavin.rocks"

#: ui/loginflow/AddPipedPage.qml:76
#, kde-format
msgctxt "Add instance"
msgid "Add %1"
msgstr "הוספת %1"

#: ui/loginflow/WelcomePage.qml:13 ui/settings/SourcesPage.qml:24
#, kde-format
msgctxt "@title:window"
msgid "Welcome"
msgstr "ברוך בואך"

#: ui/loginflow/WelcomePage.qml:25
#, kde-format
msgid "Welcome to PlasmaTube"
msgstr "ברוך בואך ל־PlasmaTube"

#: ui/loginflow/WelcomePage.qml:31
#, kde-format
msgid "PlasmaTube requires at least one video source."
msgstr "ל־PlasmaTube נדרש לפחות מקור סרטונים אחד."

#: ui/loginflow/WelcomePage.qml:41
#, kde-format
msgid "Add Invidious Source"
msgstr "הוספת מקור Invidious"

#: ui/loginflow/WelcomePage.qml:53
#, kde-format
msgid "Add PeerTube Source"
msgstr "הוספת מקור PeerTube"

#: ui/loginflow/WelcomePage.qml:65
#, kde-format
msgid "Add Piped Source"
msgstr "הוספת מקור Piped"

#: ui/LoginPage.qml:23
#, kde-format
msgctxt "@title:window"
msgid "Log in"
msgstr "כניסה"

#: ui/LoginPage.qml:68
#, kde-format
msgid "Username"
msgstr "שם משתמש"

#: ui/LoginPage.qml:82 ui/settings/NetworkProxyPage.qml:91
#, kde-format
msgid "Password"
msgstr "סיסמה"

#: ui/LoginPage.qml:97
#, kde-format
msgid "Log in"
msgstr "כניסה"

#: ui/LoginPage.qml:105
#, kde-format
msgid "Username must not be empty!"
msgstr "שם המשתמש לא יכול להישאר ריק!"

#: ui/LoginPage.qml:110
#, kde-format
msgid "Password must not be empty!"
msgstr "הסיסמה לא יכולה להישאר ריקה!"

#: ui/Main.qml:215
#, kde-format
msgctxt "@title"
msgid "Add to Playlist"
msgstr "הוספה לרשימת נגינה"

#: ui/PlaylistPage.qml:42
#, kde-format
msgctxt "@action:button Play the playlist"
msgid "Play"
msgstr "נגינה"

#: ui/SearchPage.qml:89
#, kde-format
msgid "Sort By:"
msgstr "מיון לפי:"

#: ui/SearchPage.qml:93
#, kde-format
msgid "Rating"
msgstr "דירוג"

#: ui/SearchPage.qml:105
#, kde-format
msgid "Relevance"
msgstr "התאמה"

#: ui/SearchPage.qml:118
#, kde-format
msgid "Upload Date"
msgstr "תאריך העלאה"

#: ui/SearchPage.qml:130
#, kde-format
msgid "View Count"
msgstr "מדד צפיות"

#: ui/settings/AccountCard.qml:22
#, kde-format
msgid "Currently logged in as %1."
msgstr "נכנסת בתור %1."

#: ui/settings/AccountCard.qml:39
#, kde-format
msgctxt "@action:button"
msgid "Log out"
msgstr "יציאה"

#: ui/settings/AccountCard.qml:41
#, kde-format
msgctxt "@action:button"
msgid "Log in"
msgstr "כניסה"

#: ui/settings/AccountCard.qml:45
#, kde-format
msgid "Subscribe to channels, keep track of watched videos and more."
msgstr "הרשמה לערוצים, מעקב אחר הסרטונים שנצפו ועוד."

#: ui/settings/BaseSourcePage.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Edit Source"
msgstr "עריכת מקור"

#: ui/settings/BaseSourcePage.qml:20
#, kde-format
msgid "Remove Source…"
msgstr "הסרת מקור…"

#: ui/settings/BaseSourcePage.qml:21
#, kde-format
msgid "Cannot remove the only source."
msgstr "לא ניתן להסיר את המקור היחיד שנותר."

#: ui/settings/BaseSourcePage.qml:32
#, kde-format
msgctxt "@title"
msgid "Remove Source"
msgstr "הסרת מקור"

#: ui/settings/BaseSourcePage.qml:33
#, kde-format
msgctxt "@label"
msgid "Are you sure you want to remove this source?"
msgstr "להסיר את המקור הזה?"

#: ui/settings/GeneralPage.qml:14 ui/settings/SettingsPage.qml:15
#, kde-format
msgid "General"
msgstr "כללי"

#: ui/settings/GeneralPage.qml:21
#, kde-format
msgctxt "@option:check Hide short-form videos"
msgid "Hide Shorts"
msgstr "הסתרת Shorts"

#: ui/settings/GeneralPage.qml:22
#, kde-format
msgid "Hide short-form videos designed for viewing on a mobile device."
msgstr "הסתרת סרטונים בסגנון Shorts שנועדו לצפייה במכשירים ניידים."

#: ui/settings/InvidiousSourcePage.qml:15 ui/settings/PeerTubeSourcePage.qml:11
#, kde-format
msgid "Account"
msgstr "חשבון"

#: ui/settings/InvidiousSourcePage.qml:37
#, kde-format
msgid "Preferences"
msgstr "העדפות"

#: ui/settings/InvidiousSourcePage.qml:42
#, kde-format
msgid "Autoplay videos"
msgstr "נגינת סרטונים אוטומטית"

#: ui/settings/InvidiousSourcePage.qml:43
#, kde-format
msgid ""
"Whether to start playing videos immediately, or wait until the play button "
"is pressed."
msgstr "האם להתחיל לנגן סרטונים מיידית או להמתין ללחיצה על כפתור הנגינה."

#: ui/settings/InvidiousSourcePage.qml:58
#, kde-format
msgid "Default homepage"
msgstr "דף הבית כברירת מחדל"

#: ui/settings/InvidiousSourcePage.qml:59
#, kde-format
msgid "The default page to load when switching to this source."
msgstr "עמוד ברירת המחדל לטעינה במעבר למקור הזה."

#: ui/settings/NetworkProxyPage.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Network Proxy"
msgstr "מתווך רשת"

#: ui/settings/NetworkProxyPage.qml:27
#, kde-format
msgid "System Default"
msgstr "ברירת המחדל של המערכת"

#: ui/settings/NetworkProxyPage.qml:37
#, kde-format
msgid "HTTP"
msgstr "HTTP"

#: ui/settings/NetworkProxyPage.qml:53
#, kde-format
msgid "Host"
msgstr "מארח"

#: ui/settings/NetworkProxyPage.qml:64
#, kde-format
msgid "Port"
msgstr "פתחה"

#: ui/settings/NetworkProxyPage.qml:83
#, kde-format
msgid "User"
msgstr "משתמש"

#: ui/settings/NetworkProxyPage.qml:107
#, kde-format
msgid "Apply"
msgstr "החלה"

#: ui/settings/SettingsPage.qml:21 ui/settings/SourcesPage.qml:18
#, kde-format
msgid "Sources"
msgstr "מקורות"

#: ui/settings/SettingsPage.qml:27
#, kde-format
msgid "Network Proxy"
msgstr "מתווך רשת"

#: ui/settings/SettingsPage.qml:33
#, kde-format
msgid "About PlasmaTube"
msgstr "על PlasmaTube"

#: ui/settings/SettingsPage.qml:39
#, kde-format
msgid "About KDE"
msgstr "על KDE"

#: ui/settings/SourcesPage.qml:22
#, kde-format
msgctxt "@action:button"
msgid "Add Source…"
msgstr "הוספת מקור…"

#: ui/SubscriptionListPage.qml:25
#, kde-format
msgctxt "@title:window List of subscribed channels"
msgid "Subscribed Channels"
msgstr "ערוצים שנרשמת אליהם"

#: ui/SubscriptionListPage.qml:34
#, kde-format
msgid "Importing Completed"
msgstr "הייבוא הושלם"

#: ui/SubscriptionListPage.qml:34
#, kde-format
msgid "Exporting Completed"
msgstr "הייצוא הושלם"

#: ui/SubscriptionListPage.qml:37
#, kde-format
msgid "Imported %1 new subscription."
msgid_plural "Imported %1 new subscriptions."
msgstr[0] "יובא מינוי חדש."
msgstr[1] "יובאו שני מינויים חדשים."
msgstr[2] "יובאו %1 מינויים חדשים."
msgstr[3] "יובאו %1 מינויים חדשים."

#: ui/SubscriptionListPage.qml:39
#, kde-format
msgid "All imported channels were already subscribed to."
msgstr "כבר נרשמת לכל הערוצים המיובאים."

#: ui/SubscriptionListPage.qml:42
#, kde-format
msgid "Exported %1 subscription."
msgid_plural "Exported %1 subscriptions."
msgstr[0] "יוצא מינוי."
msgstr[1] "יוצאו שני מינויים."
msgstr[2] "יוצאו %1 מינויים."
msgstr[3] "יוצאו %1 מינויים."

#: ui/SubscriptionListPage.qml:51
#, kde-format
msgctxt "@title:window Error while import/export"
msgid "Error"
msgstr "שגיאה"

#: ui/SubscriptionListPage.qml:59
#, kde-format
msgctxt "@action:button Import subscriptions"
msgid "Import…"
msgstr "ייבוא…"

#: ui/SubscriptionListPage.qml:64
#, kde-format
msgctxt "@title:window Import the OPML file"
msgid "Import OPML Subscriptions"
msgstr "ייבוא מינויים ב־OPML"

#: ui/SubscriptionListPage.qml:65
#, kde-format
msgctxt "@action:button Import the OPML file"
msgid "Import"
msgstr "ייבוא"

#: ui/SubscriptionListPage.qml:70
#, kde-format
msgctxt "@action:button Export subscriptions"
msgid "Export…"
msgstr "ייצוא…"

#: ui/SubscriptionListPage.qml:75
#, kde-format
msgctxt "@title:window Export the OPML file"
msgid "Export OPML Subscriptions"
msgstr "ייצוא מינויים ב־OPML"

#: ui/SubscriptionListPage.qml:76
#, kde-format
msgctxt "@action:button Export the OPML file"
msgid "Export"
msgstr "ייצוא"

#: ui/SubscriptionListPage.qml:103
#, kde-format
msgid "Importing…"
msgstr "מתבצע ייבוא…"

#: ui/SubscriptionListPage.qml:103
#, kde-format
msgid "Exporting…"
msgstr "מתבצע ייצוא…"

#: ui/SubscriptionListPage.qml:179
#, kde-format
msgid "No channels"
msgstr "אין ערוצים"

#: ui/SubscriptionsPage.qml:12
#, kde-format
msgctxt "@action:button View subscribed channels"
msgid "Subscribed Channels"
msgstr "ערוצים שנרשמת אליהם"

#: ui/TrendingPage.qml:15
#, kde-format
msgctxt "@title Trending videos"
msgid "Trending"
msgstr "מובילים"

#: ui/videoplayer/PictureInPictureVideo.qml:14
#, kde-format
msgid "Picture in Picture"
msgstr "חלונית צפה"

#: ui/videoplayer/VideoControls.qml:103
#, kde-format
msgctxt "@action:button"
msgid "Video settings"
msgstr "הגדרות סרטון"

#: ui/videoplayer/VideoControls.qml:132
#, kde-format
msgctxt "@action:button"
msgid "Seek backward"
msgstr "חתירה אחורה"

#: ui/videoplayer/VideoControls.qml:173
#, kde-format
msgctxt "@action:button"
msgid "Play"
msgstr "נגינה"

#: ui/videoplayer/VideoControls.qml:217
#, kde-format
msgctxt "@action:button"
msgid "Seek forward"
msgstr "חתירה קדימה"

#: ui/videoplayer/VideoControls.qml:262
#, kde-format
msgctxt "@action:button"
msgid "Fullscreen"
msgstr "מסך מלא"

#: ui/videoplayer/VideoControls.qml:285
#, kde-format
msgctxt "@action:button"
msgid "Picture-in-picture"
msgstr "חלונית צפה"

#: ui/videoplayer/VideoPlayer.qml:111
#, kde-format
msgctxt "@action:button"
msgid "Hide player"
msgstr "הסתרת הנגן"

#: ui/videoplayer/VideoPlayer.qml:133
#, kde-format
msgctxt "@action:button"
msgid "Add to Playlist…"
msgstr "הוספה לרשימת נגינה…"

#: ui/videoplayer/VideoPlayer.qml:287
#, kde-format
msgid "Copy Video URL"
msgstr "העתקת כתובת סרטון"

#: ui/videoplayer/VideoPlayer.qml:295
#, kde-format
msgid "Video Statistics"
msgstr "סטטיסטיקת סרטון"

#: ui/videoplayer/VideoPlayer.qml:386
#, kde-format
msgid "%1 Likes"
msgstr "%1 לייקים"

#: ui/videoplayer/VideoPlayer.qml:435
#, kde-format
msgid "Comments"
msgstr "תגובות"

#: ui/videoplayer/VideoPlayer.qml:465
#, kde-format
msgid "Recommended"
msgstr "מומלצים"

#: ui/videoplayer/VideoQueueView.qml:35
#, kde-format
msgid "Queue"
msgstr "תור"

#: ui/videoplayer/VideoQueueView.qml:44
#, kde-format
msgctxt "@action:button Clear playlist"
msgid "Clear"
msgstr "פינוי"

#: ui/videoplayer/VideoQueueView.qml:81
#, kde-format
msgid "Loading…"
msgstr "בטעינה…"

#~ msgid "%1 views • %2"
#~ msgstr "%1 צפיות • %2"

#~ msgid "© Linus Jahn"
#~ msgstr "© לינוס יאן"

#~ msgid "Sign in"
#~ msgstr "כניסה"

#~ msgid "Successfully logged in!"
#~ msgstr "נכנסת בהצלחה!"

#~ msgctxt "@action:button"
#~ msgid "Add Source"
#~ msgstr "הוספת מקור"
